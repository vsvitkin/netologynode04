/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 4
*/

//Часть 2
const fs = require('fs');
const crypto = require('crypto');

const input = fs.createReadStream('file1.txt');
const output = fs.createWriteStream('file2.txt');
const hash = crypto.createHash('md5');

const Transform = require('stream').Transform;

const transformator = new Transform( { objectMode: true } );
    transformator._transform = function (chunk, encoding, done) {
    this.push(chunk.toString('hex'));
    done()
}

transformator.pipe(process.stdout);
input.pipe(hash).pipe(transformator).pipe(output);
