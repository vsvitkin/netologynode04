/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 4
*/

//Часть 1
const fs = require('fs');
const crypto = require('crypto');

const input = fs.createReadStream('file1.txt');
const output = fs.createWriteStream('file2.txt');
const hash = crypto.createHash('md5');
hash.setEncoding('hex');

const hashed = input.pipe(hash);

hashed.pipe(process.stdout);
hashed.pipe(output);
