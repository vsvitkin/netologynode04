/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 4
*/

//Дополнительное задание
const Readable  = require('stream').Readable;
const Writable  = require('stream').Writable;
const Transform = require('stream').Transform;

const read = new Readable();
const trans = new Transform( { objectMode: true } );
const write = new Writable();

trans._transform = function (chunk, encoding, done) {
  setTimeout(function () {
    trans.push(chunk.toString()+' :) ');
    done();
  }, 1000);
}

read._read = function(){
  this.push(Math.round(Math.random()*9).toString());
}

write._write = function(chunk, enc, next){
  console.log(chunk.toString());
  next();
}
//read.pipe(trans).pipe(process.stdout);
read.pipe(trans).pipe(write);
